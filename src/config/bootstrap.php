<?php
Yii::setAlias('@config', __DIR__);
Yii::setAlias('@app', dirname(__DIR__));
Yii::setAlias('@extensions', dirname(__DIR__).'/extensions');
Yii::setAlias('@root', dirname(dirname(__DIR__)));
Yii::setAlias('@base', dirname(dirname(__DIR__)));
Yii::setAlias('@vendor', dirname(dirname(__DIR__)) . '/vendor');
Yii::setAlias('@bower', dirname(dirname(__DIR__)) . '/vendor/bower-asset');
Yii::setAlias('@runtime', dirname(dirname(__DIR__)) . '/runtime');
Yii::setAlias('@download', dirname(dirname(__DIR__)) . '/storage/download');
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('@web', '/');
Yii::setAlias('@storage', dirname(dirname(__DIR__)) . '/storage');