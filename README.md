<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">OpenADM Basic Project Template Based Yii2</h1>
    <br>
</p>

[OpenADM](http://www.openadm.com/) Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating an extensible project.

The template contains the basic features including user management,RBAC and extension management.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

DIRECTORY STRUCTURE
-------------------

      environments/       contains environment-based overrides
      src/                contains all application's code
      runtime/            contains files generated during runtime
      tools/              contains tools for compress application's assets
      vendor/             contains dependent 3rd-party packages
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.0.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar create-project --prefer-dist --stability=dev openadm/yii2-basic-app basic
~~~

CONFIGURATION
-------------

### Database

Edit the file `environments/dev/src/config/db.php` with real data for DEV environment:

```php
<?php

$config = [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=yii2',
            'username' => 'root',
            'password' => '123456',
            'charset' => 'utf8',
            'tablePrefix' => 'oa_',
        ],
    ],
];

return $config;

```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.


RUN
-------------

- Init the project with the command 'init', enter in the directory 'baisc'
```
./init
```
choose the [1] for DEV environment.

- Migrate DB
```
/yii migrate --migrationPath=@openadm/admin/migrations
./yii migrate all -p=@tecnocen/oauth2server/migrations/tables
```




- Now you should be able to access the application through the following URL, assuming `basic/web` is the directory
directly under the Web root of http://yii2demo.openadm.com.

~~~
http://yii2demo.openadm.com
~~~